import * as appStructure from '../../config/appStructure';
import $ from './../../../node_modules/cash-dom';

// Services
import HttpService from '../services/HttpService';

// Controllers
import ContentController from './controllers/ContentController';

const KEY_CONTROLLERS = 'controllers';
const KEY_SERVICES = 'services';

class AppContainer {
  constructor() {
    this.initOptions = {};

    Object.keys(appStructure).forEach((key) => {
      this[key] = {};
      this.initOptions[key] = appStructure[key];
    });
  }

  init() {
    const initEventReady = new CustomEvent(AppContainer.initEvent);
    const serviceInitOpts = this.initOptions[KEY_SERVICES];
    const ctrlInitOpts = this.initOptions[KEY_CONTROLLERS];

    // Services
    const servicesPool = {
      Http: new HttpService(this, serviceInitOpts['Http'] || {})
    };

    Object.keys(servicesPool).forEach((key) => this[KEY_SERVICES][key] = servicesPool[key]);

    // Controllers
    const controllersPool = {
      ContentController: new ContentController(this, ctrlInitOpts['Content'] || {})
    };

    Object.keys(controllersPool).forEach((key) => this[KEY_CONTROLLERS][key] = controllersPool[key]);

    document.dispatchEvent(initEventReady);
  }

  start() {
    $('#searchApp').removeClass('nada');
  }
}

AppContainer.initEvent = 'INIT_READY';

export default AppContainer;
