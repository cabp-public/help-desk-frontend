import $ from './../../../../node_modules/cash-dom';

const BTN_SUBMIT = 'btnSubmit';
const EL_RESULTS = 'elResults';
const EL_LOADER = 'elLoader';
const EL_INPUT_DESK = 'elInputDesk';
const EL_INPUT_FILTER = 'elInputFilter';
const SERVICES = 'services';
const PAGE_SIZE = 5;

class ContentController {
  constructor(container, options) {
    this.container = container;
    this.options = options;
    this.selectors = this.options.selectors;
    this.currentDesk = 0;
    this.currentPage = 0;
    this.results = [];
    this.Http = container[SERVICES]['Http'];
    this.resultsContainer = $(this.selectors[EL_RESULTS]);
    this.inputFilter = $(this.selectors[EL_INPUT_FILTER]);
    this.elLoader = $(this.selectors[EL_LOADER]);

    this.cleanResults = this.cleanResults.bind(this);
    this.getPage = this.getPage.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onFilter = this.onFilter.bind(this);
    this.onPageClick = this.onPageClick.bind(this);
    this.renderPageNumbers = this.renderPageNumbers.bind(this);
    this.renderRows = this.renderRows.bind(this);
    this.renderResults = this.renderResults.bind(this);
    this.toggleLoader = this.toggleLoader.bind(this);

    this.cleanResults();
    $(this.selectors[BTN_SUBMIT]).on('click', this.onSubmit);
    this.inputFilter.on('keyup', this.onFilter);
  }

  toggleLoader(on = true) {
    if (on) {
      this.elLoader.removeClass('display-none');
      this.resultsContainer.addClass('display-none');
    }
    else {
      this.elLoader.addClass('display-none');
      this.resultsContainer.removeClass('display-none');
    }
  }

  cleanResults() {
    this.resultsContainer.html('');
    this.inputFilter.val('');
  }

  getPage(page) {
    this.toggleLoader(true);

    let url = 'desk/' + this.currentDesk + '/incidents/paged/' + PAGE_SIZE + '/' + page;
    this.Http.get(url, this.renderResults);
  }

  onSubmit(e) {
    this.currentDesk = $(this.selectors[EL_INPUT_DESK]).val();
    this.currentPage = 0;
    this.getPage(0);
  }

  onPageClick(event) {
    let page = Number($(event.target).attr('data-page'));

    if (page !== this.currentPage) {
      this.getPage(page);
    }
  }

  renderPageNumbers(pages) {
    let elPages = $('div.row.pages');
    let html = '';
    let className = '';

    elPages.html(html);

    for (let p = 0; p < pages; p++) {
      className = 'grey darken' + (p === this.currentPage ? '-2' : '');

      html += '<button data-page="' + p + '" class="btn-page btn-small ' + className + '">';
      html += (p + 1);
      html += '</button>';
    }

    elPages.append($(html));
    $('button.btn-page').on('click', this.onPageClick);
  }

  renderRows(data) {
    let row, td, className;
    let tds = ['id', 'title', 'description'];

    data.forEach((incident) => {
      row = $('<tr class=""></tr>');

      tds.forEach((key) => {
        className = key === 'id' ? '' : ' class="searchable"';

        td = $('<td' + className + '></td>');
        td.html(incident[key]);
        row.append(td);
      });

      this.resultsContainer.append(row);
    });
  }

  renderResults(data) {
    this.currentPage = Number(data['details']['current']);
    this.results = data['details']['data'];

    this.cleanResults();
    this.renderPageNumbers(data['details']['pages']);
    this.renderRows(this.results);
    this.toggleLoader(false);
  }

  onFilter(event) {
    let allRows = this.resultsContainer.children('tr');
    let tds;
    let keywords = this.inputFilter.val().trim();
    let findings, found, html;

    if (keywords.length > 0) {
      allRows.each((index, tr) => {
        $(tr).removeClass('display-none');
        tds = $(tr).children('td.searchable');
        findings = [];

        tds.each((key, td) => {
          html = $(td).html().toLowerCase();
          found = html.indexOf(keywords);

          if (found >= 0) {
            findings.push(found >= 0);
          }
        });

        if (findings.length === 0) {
          $(tr).addClass('display-none');
        }
      });
    }
    else {
      allRows.each((index, tr) => {
        $(tr).removeClass('display-none');
      });
    }
  }
}

export default ContentController;
