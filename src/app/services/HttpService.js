class HttpService {
  constructor(container, options) {
    this.container = container;
    this.options = options;
  }

  get(url, callback) {
    let fullUrl = this.options['protocol'] + '://' + this.options['host'];
    fullUrl += this.options['baseUrl'] + url;

    fetch(fullUrl)
      .then((response) => {
        if ( !response.ok ) {
          throw Error(response.statusText);
        }

        return response.json();
      })
      .then((json) => {
        callback !== false && callback(json);
      })
      .catch((error) => {
        console.error(error);
      });
  }

  post() {}

  put() {}

  delete() {}
}

export default HttpService;