import AppContainer from './app/';
import './../node_modules/materialize-css/dist/css/materialize.min.css';
import './assets/css/custom.css';

const { constructor: { initEvent } } = AppContainer;

document.addEventListener(initEvent, function (event) {
  AppContainer.start();
});

AppContainer.init();
