# InvGate Test
Coding example for InvGate (front-end)

## The Approach
The code was written using [Babel](https://babeljs.io/) and [Webpack](https://webpack.js.org/) to transpile ECMA6 and package loading. Th `AppContainer.js` file is a simple approach to simulate a container for reusable classes and services. You'll notice that all classes are using bindings instead of arrow functions, that's because arrow functions as class properties impact the application performance:

> We know that usual functions are defined in the prototype and will be shared across all instances. If we have a list of N components, these components will share the same method. So, if our components get clicked we still call our method N times, but it will call the same prototype. As we’re calling the same method multiple times across the prototype, the JavaScript engine can optimize it.

Nicolas Charpentier's article [Arrow Functions in Class Properties Might Not Be As Great As We Think](https://medium.com/@charpeni/arrow-functions-in-class-properties-might-not-be-as-great-as-we-think-3b3551c440b1)

## The Challenge
The challenge includes a very simple data-loading and filter functionality. The most simple approach for loading big amounts of data is clearly pagination, demonstrated in this project. When loading incidents for a specific help-desk the script will request to the back-end to load only 5 incidents at a time, the response from the back-end includes the total number of pages and all incident for the current one. Using the response data the font-end script renders results (using an html table) and several buttons to load other pages.

## How to install
You'll need NodeJS for your development environment. Create a folder in your `web-pub` directory and clone this repository in it, open your console and run `npm install`. The npm package manager will install all required packages for both production and development, next simply run `npm run start` and navigate in your browser to `http://localhost:8080/`. As you might already noticed, you'll need the [InvGate API Project](https://bitbucket.org/cabp-ec/ivgate-test/src/master/).
